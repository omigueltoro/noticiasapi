package com.noticiasapi.controller;

import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.noticiasapi.commons.IGenericRepository;
import com.noticiasapi.models.Autor;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class AutoresController {
	
	@Autowired
	private IGenericRepository<Autor> repository;
	
	@GetMapping(value="/autores")
	public List<Autor> getAll(){
		Comparator<Autor> comparator = new Comparator<Autor>() {
			@Override
			public int compare(Autor left, Autor right) {
				return left.getId() - right.getId();
			}
		};
		List<Autor> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/autores/{id}")
	public Autor find(@PathVariable Integer id) {

		Autor autor = repository.get(id);

		return autor;
	}
	@PostMapping(value="/autores")
	public ResponseEntity<Autor> save(@RequestBody Autor autor){
		
		Autor obj = repository.save(autor);
		return new ResponseEntity<Autor>(obj, HttpStatus.OK);		
		
	}	
	
	@DeleteMapping(value="/autores/{id}")
	public ResponseEntity<Autor> delete(@PathVariable Integer id){
		
		Autor autor = repository.get(id);
		if(autor != null) {
			repository.delete(autor);
		}else {
			return new ResponseEntity<Autor>(autor, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<Autor>(autor, HttpStatus.OK);	
		
	}	

}
