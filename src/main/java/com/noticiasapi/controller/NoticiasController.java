package com.noticiasapi.controller;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.noticiasapi.models.Noticia;
import com.noticiasapi.repositories.INoticiaRepository;

import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class NoticiasController {
	
	@Autowired
	private INoticiaRepository repository;
	
	@GetMapping(value="/noticias")
	public List<Noticia> getAll(){
		Comparator<Noticia> comparator = new Comparator<Noticia>() {
			@Override
			public int compare(Noticia left, Noticia right) {
				return left.getId() - right.getId();
			}
		};

		List<Noticia> list = repository.findAll();
		list.sort(comparator);

		return list;
	}

	@GetMapping(value = "/noticias/ultimas")
	public List<Noticia> getLast() {

		Pageable ultimosSeis = PageRequest.of(0, 6, Sort.by("id").descending());

		Page<Noticia> page = repository.findAll(ultimosSeis);

		List<Noticia> list = page.getContent().stream()
									.collect(Collectors.toList());

		return list;
	}

	@GetMapping(value = "/noticias/categoria/{categoriaId}")
	public List<Noticia> getCategoria(@PathVariable Integer categoriaId) {
		Comparator<Noticia> comparator = new Comparator<Noticia>() {
			@Override
			public int compare(Noticia left, Noticia right) {
				return left.getId() - right.getId();
			}
		};

		List<Noticia> list = repository.findAll().stream()
								.filter(x -> x.getCategoria().getId() == categoriaId)
								.sorted(comparator)
								.collect(Collectors.toList());

		return list;
	}

	@GetMapping(value = "/noticias/{id}")
	public Noticia find(@PathVariable Integer id) {

		Noticia autor = repository.get(id);

		return autor;
	}

	@PostMapping(value = "/noticias")
	public ResponseEntity<Noticia> save(@RequestBody Noticia autor) {

		Noticia obj = repository.save(autor);
		return new ResponseEntity<Noticia>(obj, HttpStatus.OK);

	}

	@DeleteMapping(value = "/noticias/{id}")
	public ResponseEntity<Noticia> delete(@PathVariable Integer id) {

		Noticia autor = repository.get(id);
		if(autor != null) {
			repository.delete(autor);
		}else {
			return new ResponseEntity<Noticia>(autor, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<Noticia>(autor, HttpStatus.OK);	
		
	}	

}
