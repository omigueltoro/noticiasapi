package com.noticiasapi.controller;

import java.util.Comparator;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.noticiasapi.commons.IGenericRepository;
import com.noticiasapi.models.Categoria;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin
@RestController
@RequestMapping(value= "/api/v1/")
public class CategoriasController {
	
	@Autowired
	private IGenericRepository<Categoria> repository;
	
	@GetMapping(value="/categorias")
	public List<Categoria> getAll(){
		Comparator<Categoria> comparator = new Comparator<Categoria>() {
			@Override
			public int compare(Categoria left, Categoria right) {
				return left.getId() - right.getId();
			}
		};
		List<Categoria> list = repository.findAll();
		list.sort(comparator);

		return list;
	}
	
	@GetMapping(value="/categorias/{id}")
	public Categoria find(@PathVariable Integer id) {

		Categoria categoria = repository.get(id);

		return categoria;
	}
	@PostMapping(value="/categorias")
	public ResponseEntity<Categoria> save(@RequestBody Categoria categoria){
		
		Categoria obj = repository.save(categoria);
		return new ResponseEntity<Categoria>(obj, HttpStatus.OK);		
		
	}	
	
	@DeleteMapping(value="/categorias/{id}")
	public ResponseEntity<Categoria> delete(@PathVariable Integer id){
		
		Categoria categoria = repository.get(id);
		if(categoria != null) {
			repository.delete(categoria);
		}else {
			return new ResponseEntity<Categoria>(categoria, HttpStatus.NOT_FOUND);	 
		}
		return new ResponseEntity<Categoria>(categoria, HttpStatus.OK);	
		
	}	

}
