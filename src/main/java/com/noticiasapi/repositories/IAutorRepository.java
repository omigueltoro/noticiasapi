/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noticiasapi.repositories;
import com.noticiasapi.commons.IGenericRepository;
import com.noticiasapi.models.Autor;
import org.springframework.stereotype.Repository;
/**
 *
 * @author wancho
 */
@Repository 
public interface IAutorRepository extends IGenericRepository <Autor> {

}
