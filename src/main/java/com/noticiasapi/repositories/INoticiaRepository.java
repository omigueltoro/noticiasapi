/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.noticiasapi.repositories;

import com.noticiasapi.commons.IGenericRepository;
import com.noticiasapi.models.Noticia;
import org.springframework.stereotype.Repository;
/**
 *
 * @author mauriciomosquera
 */

@Repository 
public interface INoticiaRepository extends IGenericRepository <Noticia> {
        
}
